class Solution(object):

    def array_pair_sum(self, arr):
        l = len(arr)
        arr.sort()
        sum_arr = 0
        for i in range(0, l, 2):
            sum_arr += arr[i]
        return sum_arr

